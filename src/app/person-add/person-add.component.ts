import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import Person from "../shared/models/person.model";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";

@Component({
  selector: "app-person-add",
  templateUrl: "./person-add.component.html",
  styleUrls: ["./person-add.component.css"]
})
export class PersonAddComponent implements OnInit {
  @Output() addPerson = new EventEmitter<Person>();
  newUser: FormGroup;
  constructor() {}

  ngOnInit() {
    this.newUser = new FormGroup({
      name: new FormControl({ value: "", disabled: false }, [
        Validators.required
      ]),
      surname: new FormControl({ value: "", disabled: false }, [
        Validators.required
      ]),
      phone: new FormControl({ value: "" }, [
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ])
    });
  }
  onAddPerson() {
    // console.log(this.newUser);
    // console.log("12323");
    if (typeof this.newUser.value.phone != "object") {
      let person = new Person(
        this.newUser.value.name,
        this.newUser.value.surname,
        "+7" + this.newUser.value.phone
      );
      this.newUser.reset();
      this.addPerson.emit(person);
    }
  }
  dis = false;
}
