import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import Person from "../shared/models/person.model";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-person-view",
  templateUrl: "./person-view.component.html",
  styleUrls: ["./person-view.component.css"]
})
export class PersonViewComponent implements OnInit {
  @Input() person: Person;
  @Output() delete = new EventEmitter<number>();
  @Output() editPerson = new EventEmitter<Person>();
  updUser: FormGroup;
  nowEdit = false;

  constructor() {}
  onDelete(id: number) {
    this.delete.emit(id);
  }
  onEdit(personId) {
    let phone;
    if (this.nowEdit) {
      if (this.updUser.value.phone[0] == "+") {
        phone = this.updUser.value.phone;
      } else {
        phone = "+7" + this.updUser.value.phone;
      }
      let editedPerson = new Person(
        this.updUser.value.firstName,
        this.updUser.value.lastName,
        phone,
        personId
      );
      this.editPerson.emit(editedPerson);
    }
    this.nowEdit = !this.nowEdit;
  }
  ngOnInit() {
    this.updUser = new FormGroup({
      firstName: new FormControl(this.person.firstName, [Validators.required]),
      lastName: new FormControl(this.person.lastName, [Validators.required]),
      phone: new FormControl(this.person.phone, [
        Validators.required,
        Validators.maxLength(7)
      ])
    });
  }
}
