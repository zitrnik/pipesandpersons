import { Component, OnInit, OnDestroy } from "@angular/core";
import Person from "./shared/models/person.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  title = "Контакты";
  searchStr = "";
  persons: Person[] = [];
  constructor() {}
  ngOnInit(): void {
    // for (let index = 0; index < 5; index++) {

    this.persons.push(
      new Person(
        "Фёдор",
        "Иванов",
        "+7" + Math.ceil(Math.random() * 10000000000),
        1
      )
    );
    this.persons.push(
      new Person(
        "Никита",
        "Глазков",
        "+7" + Math.ceil(Math.random() * 10000000000),
        2
      )
    );
    this.persons.push(
      new Person(
        "Миклуха",
        "Маклай",
        "+7" + Math.ceil(Math.random() * 10000000000),
        3
      )
    );
    this.persons.push(
      new Person(
        "Маккалей",
        "Калкин",
        "+7" + Math.ceil(Math.random() * 10000000000),
        4
      )
    );
    this.persons.push(
      new Person(
        "Архип",
        "Иванов",
        "+7" + Math.ceil(Math.random() * 10000000000),
        5
      )
    );
    // }
  }
  ngOnDestroy(): void {}
  onAddPerson(person) {
    let newId = +this.persons[this.persons.length - 1].id + 1;
    person.id = newId;
    this.persons.push(person);
  }
  onEditPerson(person) {
    this.persons.splice(
      this.persons.findIndex(elem => elem.id == person.id),
      1,
      person
    );
    this.searchStr = "";
  }
  deleteFromArr(id) {
    this.persons.splice(this.persons.findIndex(elem => elem.id == id), 1);
    this.searchStr = "";
  }
}
